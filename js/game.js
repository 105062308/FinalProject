var GameState = {
    preload: function(){
        game.load.image('block1','assets/item/blue block.png');
        game.load.image('block2','assets/item/green block.png');
        game.load.image('block3','assets/item/lightblue block.png');
        game.load.image('block4','assets/item/orange block.png');
        game.load.image('block5','assets/item/purple block.png');
        game.load.image('block6','assets/item/red block.png');
        game.load.image('block7','assets/item/yellow block.png');
        game.load.image('floor','assets/item/ghost block.png');
        game.load.image('blocks1','assets/item/blue_block.png');
        game.load.image('blocks2', 'assets/item/green_block.png');
        game.load.image('blocks3', 'assets/item/lightblue_block.png');
        game.load.image('blocks4', 'assets/item/orange_block.png');
        game.load.image('blocks5', 'assets/item/purple_block.png');
        game.load.image('blocks6', 'assets/item/red_block.png');
        game.load.image('blocks7', 'assets/item/yellow_block.png');
        //game.load.spritesheet('snake','assets/monster/slime-Sheet.png',32,25);
        game.load.spritesheet('snake', 'assets/monster/snake-147x94.png',147,94);
        game.load.spritesheet('player','assets/char/warrior.png',182,123);
        game.load.image('bg','assets/item/bg.png');
        game.load.image('rpg_bg','assets/background/11.png');
        //create life bar
        game.load.image('hp1', 'assets/lifebar_01.png');
        game.load.image('hp2', 'assets/lifebar_02.png');
        game.load.image('hpshell','assets/lifebar_shell.png');
    },
    create: function(){
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        //block
        this.background = game.add.sprite(0,0,'bg');

        this.block_array = ['block1', 'block2', 'block3', 'block4', 'block5', 'block6', 'block7'];
        this.check_blcok = [0,1,2,3,4,5,6];
        this.createBlock();
        this.falling = game.time.events.loop(1000, this.BlockFalling, this);
        //floor
        this.floor = game.add.group();
        this.floor.enableBody = true;
        for(var i=0;i<10;i++){
            var floor_block = game.add.sprite(i*18,18*29,'floor',0,this.floor);
            floor_block.body.immovable = true;
        }

        this.map = new Array(30);
        for(var t=0;t<29;t++){
            this.map[t] = [0,0,0,0,0,0,0,0,0,0];
        }
        this.map[29] = [1,1,1,1,1,1,1,1,1,1];
        
        this.obstacle = new Array(30);
        for (var t = 0; t < 30; t++) {
            this.obstacle[t] = new Array(10);
        }
        //the keyboard control
        this.control = {
            left: game.input.keyboard.addKey(Phaser.Keyboard.LEFT),
            right: game.input.keyboard.addKey(Phaser.Keyboard.RIGHT),
            down: game.input.keyboard.addKey(Phaser.Keyboard.DOWN),
            z: game.input.keyboard.addKey(Phaser.Keyboard.Z),
            x: game.input.keyboard.addKey(Phaser.Keyboard.X),
            space: game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
        };
        //use onDown to trigger (isDown will trigger function repeatedly)
        this.control.space.onDown.add(this.BlockControlDrop,this);
        this.control.left.onDown.add(this.BlockControlLeft,this);
        this.control.right.onDown.add(this.BlockControlRight,this);
        this.control.z.onDown.add(this.BlockContorlRotate,this);
        this.control.x.onDown.add(this.BlockContorlRotateCounterwise,this);
        if(this.control.down.isDown){
            this.BlockControlDown();
        }
       
        this.stage_now = 1;
        
        this.rpg_bg = game.add.sprite(180,0,'rpg_bg');
        this.rpg_bg.scale.setTo(0.65,0.65);
        //player 
        this.player = game.add.sprite(380,120,'player');
        this.player.scale.setTo(-1,1);
        this.player.level = 1;
        this.player.life = 15;

        this.player.animations.add('idle',[0,1,2,3,4,5],8,true);
        this.player.animations.add('attack',[6,7,8,9,10,11],8,false);
        this.player.animations.add('guard',[12,13,14,15,16,17,18,19,20],8,false);
        this.player.animations.add('damage',[])
        this.player.animations.play('guard');
        //monster create
        this.snake = game.add.sprite(490,150,'snake');
        this.snake.scale.setTo(-0.8,0.8);
        this.snake.life = 10;
        this.snake.animations.add('idle',[0,1,2,3,4,5],8,true);
       
        this.snake.animations.play('idle');
       //this.snake.anchor.setTo(0.5,0.5);
        if(this.stage_now == 1){
            game.time.events.loop(5000,this.MonsterAutoAttack,this);
        }

        //life bar
        this.shell = game.add.sprite(180, 295, 'hpshell');
        this.shell.scale.setTo(0.4,0.5);
        this.healthbar1 = game.add.sprite(180, 295,'hp1');
        this.healthbar1.scale.setTo(0.4,0.5);
        this.healthbar2 = game.add.sprite(180 + 18*10.3, 295,'hp2');
        this.healthbar2.scale.setTo(0.4,0.5);

    },
    update: function(){
        this.checkBounds();
        this.checkPosition();
        this.lifeControl();
    },
    checkBounds: function(){  
        if(this.new_block.onLand){
            var lose = false;
            this.new_block.forEach(function (child) {
                /*console.log((child.position.y + this.new_block.position.y) / 18);
                console.log((child.position.x + this.new_block.position.x) / 18);*/
                var y = (child.position.y + this.new_block.position.y) / 18;
                var x = (child.position.x + this.new_block.position.x) / 18;
                child.position.y = y * 18;
                if(y < 0){
                    this.LoseGame();
                    lose = true;
                    return;
                }
                this.map[y][x] = 1;
                this.obstacle[y][x] = child;
            }, this);
            this.new_block.position.y = 0;
            if(this.check_block.length!=0){
                this.check_block.kill();
            }
            //console.log(this.obstacle);
            if(!lose){
                this.checkClear();
                game.time.events.pause(this.falling);
                this.createBlock();
                game.time.events.resume(this.falling);
            }
        }
    },
    createBlock: function(){
        //block rule
        this.check_block = game.add.group();
        this.number = game.rnd.pick(this.check_blcok);
        var index = this.check_blcok.indexOf(this.number);
        this.check_blcok.splice(index,1);
        if(this.check_blcok.length == 0)
            this.check_blcok = [0, 1, 2, 3, 4, 5, 6];
        //console.log(this.check_blcok);
        this.new_block = game.add.group();
        this.new_block.enableBody = true;
        //setting the block's shape
        this.createShape(this.number);
        //other setting 
        this.new_block.position.x = 90;
        this.new_block.position.y = 0;
        this.new_block.onLand = false;
    },
    createShape: function(type){
        switch(type){
            case 0:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 1:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 2:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -36, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, 18, this.block_array[type], 0, this.new_block);
                break;
            case 3:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 4:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 5:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, -18, this.block_array[type], 0, this.new_block);
                break;
            case 6:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                break;
        }
    },
    BlockControlLeft: function(){
        var canLeft = true;
        this.new_block.forEach(function(child){
            var x = (child.position.x + this.new_block.position.x ) / 18;
            var y = (child.position.y + this.new_block.position.y) / 18;
            if (y < 0) y = 0;
            if(x == 0){
                canLeft = false;
            }
            if(this.map[y][x-1]==1){
                canLeft = false;
            }
        },this);

        if(canLeft) this.new_block.position.x -= 18;
    },
    BlockControlRight: function () {
        var canRight = true;
        this.new_block.forEach(function (child) {
            var x = (child.position.x + this.new_block.position.x) / 18;
            var y = (child.position.y + this.new_block.position.y) / 18;
            if(y<0) y = 0;
            if (x == 9) {
                canRight = false;
            }
            if(this.map[y][x+1]==1){
                canRight = false;
            }
        },this);

        if(canRight) this.new_block.position.x += 18;
    },
    BlockContorlRotate: function(){
        var canrotate = true;
        this.new_block.forEach(function (child) {
            var x = (-child.position.y + this.new_block.position.x) / 18;
            if(x<0 || x>9){
                canrotate = false;
            }
        }, this);
        if(canrotate){
            this.new_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = -child.position.y;
                child.position.y = swap;
            }, this);
            this.check_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = -child.position.y;
                child.position.y = swap;
            }, this);
        }
    },
    BlockContorlRotateCounterwise: function(){
        var canrotate = true;
        this.new_block.forEach(function (child) {
            var x = (child.position.y + this.new_block.position.x) / 18;
            if (x < 0 || x > 9) {
                canrotate = false;
            }
        }, this);
        if (canrotate) {
            this.new_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = child.position.y;
                child.position.y = -swap;
            }, this);
            this.check_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = child.position.y;
                child.position.y = -swap;
            }, this);
        }
    },
    BlockControlDown: function(){
        
        var canMove = true;
        this.new_block.forEach(function (child) {
            //console.log(this.map[(child.position.y + this.new_block.position.y + 18) / 18][(child.position.x + this.new_block.position.x + 18) / 18]);
            var y = (child.position.y + this.new_block.position.y + 18) / 18;
            if (y < 0) y = 0;
            var x = (child.position.x + this.new_block.position.x) / 18;
            if (this.map[y][x] == 1) {
                canMove = false;
                return;
            }
        }, this);
        if(canMove) this.new_block.position.y += 18;
    },
    BlockControlDrop: function(){
        while(this.new_block.onLand == false){
            this.BlockFalling();
        }
        return this.new_block.position.y;
    },
    BlockFalling: function(){
        var canFall = true;
        //console.log(this.map);
        this.new_block.forEach(function(child){
            //console.log(this.map[(child.position.y + this.new_block.position.y + 18) / 18][(child.position.x + this.new_block.position.x + 18) / 18]);
            var y = (child.position.y + this.new_block.position.y + 18) / 18;
            if(y<0) y=0;
            //console.log(y);
            var x = (child.position.x + this.new_block.position.x ) / 18;
            if (this.map[y][x] == 1 ){
                this.new_block.onLand = true;
                canFall = false;
                return;
            }
        },this);
        if (canFall) {
            this.new_block.position.y += 18;
        }
    },
    checkClear: function(){
        var hit = 0;
        //console.log(this.obstacle);
        //console.log(this.map);
        //console.log(this.obastacle[28][5].world);
        for(var i=0;i<29;i++){
            if(this.map[i].indexOf(0) == -1){
                hit += 1;
                //console.log('find');
                for(var j=0;j<=9;j++){
                    this.obstacle[i][j].kill();
                    this.obstacle[i][j] = null;
                }
                this.map[i] = [0,0,0,0,0,0,0,0,0,0];
                for (var t = i; t > 0; t--) {
                    this.map[t] = this.map[t - 1];
                    for(var m=0;m<10;m++){
                        if(this.obstacle[t-1][m]){
                            this.obstacle[t-1][m].position.y += 18;
                        }
                    }
                    this.obstacle[t] = this.obstacle[t-1];
                }
            }
        }  
        if(hit!=0){
            this.playerAttack(hit);
        }
    },
    playerAttack: function(hit){
        game.add.tween(this.player).to({x:190},200).yoyo(true).start();
        game.add.tween(this.snake).to({
            tint: 0xff0000
        }, 200).easing(Phaser.Easing.Exponential.InOut).yoyo(true).start();
        if(hit == 1){
            console.log('hero deal three damage to monster');
            this.snake.life -= 3;
            console.log(this.snake.life+'/10');
        }

    },
    MonsterAutoAttack:function(){
        if(this.stage_now == 1){
            game.add.tween(this.snake).to({x:this.snake.position.x-10},200).yoyo(true).start();
            game.add.tween(this.player).to({
                tint: 0xff0000
            }, 150).easing(Phaser.Easing.Exponential.Out).yoyo(true).start();
            console.log('monster deal two damage to hero');
            this.player.life -= 2;
            console.log(this.player.life + '/15');
        }
    },
    checkPosition: function(){
        if(!this.check_block.length ){
            switch(this.number){
                case 0:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 1:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 2:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -36, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, 18, this.block_array[this.number], 0, this.check_block);
                    break;
                case 3:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 4:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 5:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, -18, this.block_array[this.number], 0, this.check_block);
                    break;
                case 6:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    break;
            }
        }
        
        this.check_block.alpha=0.3;
        this.check_block.position.x = this.new_block.position.x;
        
        this.y_final = this.new_block.position.y;
        this.checkMove = true;
        while (this.checkMove) {
            this.new_block.forEach(function (child) {
                //console.log(this.map[(child.position.y + this.new_block.position.y + 18) / 18][(child.position.x + this.new_block.position.x + 18) / 18]);
                var y = (child.position.y + this.y_final + 18) / 18;
                if (y < 0) y = 0;
                var x = (child.position.x + this.new_block.position.x) / 18;
                if (this.map[y][x] == 1) {
                    this.checkMove = false;
                    return;
                }
            }, this);
            if (this.checkMove) this.y_final += 18;
        }
        this.check_block.position.y = this.y_final;
       //  console.log(this.check_block.length);
    },

    lifeControl: function(){  
        if(this.player.life>7.5){
            this.healthbar1.width = 1 * 18*10.7;
            this.healthbar2.width = ((this.player.life-7.5) / 7.5) * 18*9.2;
        }else{
            this.healthbar1.width = ((this.player.life) / 7.5) * 18*10.7;
            this.healthbar2.width = 0 * 18*9.2;
        }
    },
    LoseGame: function(){
        game.camera.shake();
    }
};
